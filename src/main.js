import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/lib/buefy.css';

import App from './App.vue';
import router from './index';

Vue.use(Buefy);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
