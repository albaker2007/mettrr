import Vue from 'vue';
import Router from 'vue-router';

import CountryList from './countryList.vue';

Vue.use(Router);

const routes = [
    {
        path: '/country-list',
        name: 'Country List',
        component: CountryList,
    },
];

export default new Router({
    mode: 'history',
    routes,
});
